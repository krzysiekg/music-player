import { useState, useEffect } from 'react';
import useAppContext from 'hooks/useAppContext';

export default function useTracksApi(apiUrl) {
  const { setTracks } = useAppContext();
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);

      try {
        const result = await fetch(apiUrl);
        const { tracks } = await result.json();
        setTracks(tracks);
      } catch (error) {
        setIsError(true);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [apiUrl]);

  return { isLoading, isError };
}
