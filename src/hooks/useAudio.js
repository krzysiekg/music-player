import { useState, useEffect } from 'react';

export default function useAudio(url) {
  const [audio] = useState(new Audio(url));
  const [playing, setPlaying] = useState(true);
  const [second, setSecond] = useState(0);

  function toggle() {
    setPlaying(!playing);
  }

  function moveTrack(value) {
    audio.currentTime = value;
    setSecond(value);
  }

  useEffect(
    () => {
      audio.src = url;
      audio.load();
      setPlaying(true)
    },
    [url]
  )

  useEffect(
    () => {
      if (playing) {
        audio.play();
      } else {
        audio.pause();
      }
    },
    [playing, url]
  );

  useEffect(
    () => {
      audio.addEventListener('timeupdate', () => {
        setSecond(audio.currentTime)
      })
    },
    []
  )
  
  return [playing, toggle, second, moveTrack];
};
