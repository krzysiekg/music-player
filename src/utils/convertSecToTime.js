export default function convertSecToTime(second) {
  const seconds = Math.round(second % 60); 
  const minutes = Math.round((second - seconds) / 60);
  const convertedMinutes = (`0${minutes}`).slice(-2);
  const convertedSeconds = (`0${seconds}`).slice(-2);

  return `${convertedMinutes}:${convertedSeconds}`;
}
