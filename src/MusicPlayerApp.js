import React from 'react';
import useTracksApi from 'hooks/useTracksApi';
import List from 'components/List';
import Player from 'components/Player';
import Loader from 'components/Loader';
import styles from './MusicPlayerApp.module.css';

function MusicPlayerApp() {
  const { isLoading } = useTracksApi('http://api.napster.com/v2.2/tracks/top?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4&limit=10&range=month&offset=80');

  return (
    <div className={styles.container}>
      {
        isLoading ?
          <Loader /> :
          <React.Fragment>
            <Player />
            <List />
          </React.Fragment>
      }
    </div>
  );
}

export default MusicPlayerApp;
