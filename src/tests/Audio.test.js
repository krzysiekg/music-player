import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import Audio from 'components/Player/Audio';

window.HTMLMediaElement.prototype.load = () => {};
window.HTMLMediaElement.prototype.play = () => {};
window.HTMLMediaElement.prototype.pause = () => {};

const commonProps = {
  handleNextTrack: () => {},
  handlePreviousTrack: () => {},
}

const firstComponent = <Audio
  {...commonProps}
  track={{
    artistName: 'A',
    name: 'AA',
    previewURL:'https://www.mfiles.co.uk/mp3-downloads/beethoven-symphony6-1.mp3'
  }}
/>

const secondComponent = <Audio
  {...commonProps}
  track={{
    artistName: 'B',
    name: 'BB',
    previewURL:'https://www.mfiles.co.uk/mp3-downloads/haydn-symphony88-1.mp3'
  }}
/>

describe('Audio', () => {
  it('changes playing state by clicking play/pause button', () => {
    const { container } = render(firstComponent)
    const playPauseButton = container.querySelector('.play');

    expect(playPauseButton.className).toBe('fas play fa-pause');

    fireEvent.click(playPauseButton);
    expect(playPauseButton.className).toBe('fas play fa-play');
  })

  it('changes playing state after changing track', () => {
    const { container, rerender } = render(firstComponent)
    const playPauseButton = container.querySelector('.play');

    fireEvent.click(playPauseButton);
    expect(playPauseButton.className).toBe('fas play fa-play');
    
    rerender(secondComponent);

    expect(container.querySelector('.play').className).toBe('fas play fa-pause');
  })

  it('changes track and artist name after changing track', () => {
    const { container, rerender } = render(firstComponent);

    const artistName = container.querySelector('.artistName');
    const trackName = container.querySelector('.trackName');

    expect(artistName.textContent).toBe('A');
    expect(trackName.textContent).toBe('AA');
    
    rerender(secondComponent);

    expect(artistName.textContent).toBe('B');
    expect(trackName.textContent).toBe('BB');
  })
})
