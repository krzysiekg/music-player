import React from 'react';
import PropTypes from 'prop-types';
import useAudio from 'hooks/useAudio';
import TrackInfo from './TrackInfo';
import Panel from './Panel';

export default function Audio({ handleNextTrack, handlePreviousTrack, track }) {
  const [playing, toggle, second, moveTrack] = useAudio(track.previewURL);

  return (
    <React.Fragment>
      <TrackInfo track={track} />
      <Panel
        duration={30}
        handleNextTrack={handleNextTrack}
        handlePreviousTrack={handlePreviousTrack}
        moveTrack={moveTrack}
        playing={playing}
        toggle={toggle}
        second={second}
      />
    </React.Fragment>
  );
}

Audio.propTypes = {
  handleNextTrack: PropTypes.func.isRequired,
  handlePreviousTrack: PropTypes.func.isRequired,
  track: PropTypes.shape({
    previewURL: PropTypes.string.isRequired
  }).isRequired
}
