import React from 'react';
import TrackSlider from './TrackSlider';
import styles from './index.module.css';

export default function Panel(props) {
  const {
    duration, handlePreviousTrack, handleNextTrack, moveTrack, playing, second, toggle 
  } = props;
  
  return (
    <div className={styles.container}>
      <div className={styles.buttons}>
        <i className="fas fa-step-backward" onClick={handlePreviousTrack} />
        <i className={`fas ${styles.play} fa-${playing ? 'pause' : 'play'}`} onClick={toggle}/>
        <i className="fas fa-step-forward" onClick={handleNextTrack}/>
      </div>
      <TrackSlider
        duration={duration}
        moveTrack={moveTrack}
        second={second}
      />
    </div>
  );
}
