import React from 'react';
import Slider from 'react-rangeslider';
import convertSecToTime from 'utils/convertSecToTime';
import styles from './TrackSlider.module.css';
import 'react-rangeslider/lib/index.css';

export default function Panel({ second, duration, moveTrack }) {
  return (
    <div className={styles.duration}>
      <div className={styles.time}>{convertSecToTime(second)}</div>
      <div className={styles.slider}>
        <Slider
          value={second}
          max={duration}
          onChange={moveTrack}
          tooltip={false}
        />
      </div>
      <div className={styles.time}>{convertSecToTime(duration)}</div>
    </div>
  );
}
