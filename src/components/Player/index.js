import React from 'react';
import useAppContext from 'hooks/useAppContext';
import Audio from './Audio';
import styles from './index.module.css';

export default function Player() {
  const { selectTrack, track, tracks } = useAppContext();

  function handlePreviousTrack() {
    const index = tracks.findIndex(item => item.id === track.id);
    if (index === 0) return;
    selectTrack(tracks[index-1].id);
  }

  function handleNextTrack() {
    const index = tracks.findIndex(item => item.id === track.id);
    if (index === tracks.length - 1) return;
    selectTrack(tracks[index+1].id);
  }

  if (!track) {
    return <div className={styles.playerContainer} />;
  }

  return (
    <div className={styles.playerContainer}>
      <Audio
        handleNextTrack={handleNextTrack}
        handlePreviousTrack={handlePreviousTrack}
        track={track}
      />
    </div>
  );
}
