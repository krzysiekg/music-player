import React from 'react';
import PropTypes from 'prop-types';
import styles from './Info.module.css';

export default function Info({ artistName, trackName }) {
  return (
    <div className={styles.container}>
      <span className={styles.trackName}>{trackName}</span>
      <span className={styles.artistName}>{artistName}</span>
    </div>
  );
}

Info.propTypes = {
  artistName: PropTypes.string.isRequired,
  trackName: PropTypes.string.isRequired
};
