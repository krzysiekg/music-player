import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Preview from './Preview';
import Info from './Info';
import styles from './index.module.css';

function TrackInfo({ track }) {
  return (
    <div className={styles.infoContainer} >
      <Preview />
      <Info artistName={track.artistName} trackName={track.name} />
    </div>
  );
}

TrackInfo.propTypes = {
  track: PropTypes.shape({
    artistName: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired
}

export default memo(TrackInfo);
