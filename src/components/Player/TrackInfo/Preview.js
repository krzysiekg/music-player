import React from 'react';
import styles from './index.module.css'

export default function Preview() {
  return (
    <img alt="albumPreview" className={styles.preview} src={'https://via.placeholder.com/132'} />
  );
}
