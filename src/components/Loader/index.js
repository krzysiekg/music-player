import React from 'react';
import styles from './index.module.css';

export default function Loader() {
  return (
    <div className={styles.loader}>
      <div className={styles['lds-grid']}>
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
}
