import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styles from './Item.module.css';

function Item({ artistName, id, selectTrack, trackName }) {
  function handleClick() {
    selectTrack(id);
  } 

  return (
    <li className={styles.item} onClick={handleClick}>
      <img alt="albumCover" className={styles.trackImg} src={'https://via.placeholder.com/40'} />
      <div className={styles.info}>
        <span className={styles.trackName}>{trackName}</span>
        <span className={styles.artistName}>{artistName}</span>
      </div>
    </li>
  );
} 

Item.propTypes = {
  artistName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  selectTrack: PropTypes.func.isRequired,
  trackName: PropTypes.string.isRequired
}

export default memo(Item);

