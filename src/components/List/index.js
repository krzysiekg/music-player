import React, { useMemo } from 'react';
import useAppContext from 'hooks/useAppContext';
import Item from './Item';
import styles from './index.module.css';

export default function List() {
  const { selectTrack, tracks } = useAppContext();

  const render = useMemo(() => {
    return (
      <div className={styles.container}>
        <ul className={styles.list}>
          {
            tracks.map(track => {
              const { artistName, id, name } = track;

              return (
                <Item
                  key={id}
                  artistName={artistName}
                  id={id}
                  selectTrack={selectTrack}
                  trackName={name}
                />
              );
            })
          }
        </ul>
      </div>
    )
  }, [tracks]);

  return render;
}
