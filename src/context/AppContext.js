import React, { createContext, useState } from 'react';

export const AppContext = createContext({
  track: null,
  tracks: []
});

export const AppContextProvider = (props) => {
  const [track, setTrack] = useState(null);
  const [tracks, setTracks] = useState([]);

  function selectTrack(trackId) {
    const selectedTrack = tracks.find(track=> track.id === trackId);
    setTrack(selectedTrack || null)
  }

  return (
    <AppContext.Provider value={{ selectTrack, setTracks, track, tracks }}>{props.children}</AppContext.Provider>
  );
}
